extern crate gio;
extern crate gtk;

use std::env::args;

use gio::prelude::*;
use gio::{Menu, MenuItem, SimpleAction};
use gtk::prelude::*;
use gtk::{
    gdk, glib, style_context_add_provider_for_display, AboutDialog,
    Application, ApplicationWindow, Builder, CssProvider,
    STYLE_PROVIDER_PRIORITY_APPLICATION,
};

fn load_css() {
    let display =
        gdk::Display::default().expect("couldn't get default display");
    let provider = CssProvider::new();
    let priority = STYLE_PROVIDER_PRIORITY_APPLICATION;
    let css_src = include_str!("./ui/style.css");
    provider.load_from_data(css_src);
    style_context_add_provider_for_display(&display, &provider, priority);
}

fn build_ui(app: &Application) {
    let win_src = include_str!("./ui/window.xml");
    let win: ApplicationWindow = Builder::from_string(win_src)
        .object("window")
        .expect("couldn't get window");
    win.set_default_size(600, 400);
    win.set_application(Some(app));

    load_css();

    let bar = Menu::new();

    // file
    let file = MenuItem::new(Some("File"), None);
    let file_menu = Menu::new();
    let quit = SimpleAction::new("quit", None);
    app.add_action(&quit);
    file_menu.append(Some("Quit"), Some("app.quit"));
    file.set_submenu(Some(&file_menu));
    bar.append_item(&file);

    // help
    let help = MenuItem::new(Some("Help"), None);
    let help_menu = Menu::new();
    let about = SimpleAction::new("about", None);
    app.add_action(&about);
    help_menu.append(Some("About"), Some("app.about"));
    help.set_submenu(Some(&help_menu));
    bar.append_item(&help);

    app.set_menubar(Some(&bar));
    win.set_show_menubar(true);

    quit.connect_activate(glib::clone!(@weak app => move |_, _| {
        app.quit();
    }));

    about.connect_activate(glib::clone!(@weak win => move |_, _| {
        let dialog_src = include_str!("./ui/about_dialog.xml");
        // NOTE:
        // It seems that the use of AboutDialog causes a warning like:
        // Gtk-WARNING **: ... No IM module matching GTK_IM_MODULE=xxx found
        let dialog: AboutDialog = Builder::from_string(dialog_src)
            .object("about_dialog")
            .expect("couldn't get about_dialog");
        dialog.set_transient_for(Some(&win));
        dialog.present();
    }));

    win.present();
}

fn main() -> glib::ExitCode {
    let application = Application::new(
        Some("net.grauwoelfchen.gtk.desktop.motif"),
        Default::default(),
    );
    application.connect_activate(|app| {
        build_ui(app);
    });
    application.run_with_args(&args().collect::<Vec<_>>())
}
